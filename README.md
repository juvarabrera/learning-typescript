# learn-typescript

## Background

I have been exposed with Typescript with some Angular projects. I am loving static programming languages as it is readable and guides developers on what to store in a variable, the type of parameters, or the return type of functions.

## Where I am learning
I am learning Typescript on Codecademy (https://www.codecademy.com/learn/learn-typescript)

## Resources
