import { restaurants, Restaurant } from "./restaurants";
import { orders, Order, PriceBracket } from "./orders";

/// Add your getMaxPrice() function below:
let getMaxPrice = function(priceBracket: PriceBracket): number {
  if(priceBracket == PriceBracket.Low)
    return 10;
  if(priceBracket == PriceBracket.Medium)
    return 20;
  if(priceBracket == PriceBracket.High)
    return 30;
  return 0;
}
/// Add your getOrders() function below:
let getOrders = function(priceBracket: PriceBracket, orders: Order[][]) {
  let filteredOrders: Order[][] = [];
  orders.forEach(ordersFromRestaurants => {
    filteredOrders.push(ordersFromRestaurants.filter(order => {
      if(order.price < getMaxPrice(priceBracket))
        return true;
      return false;
    }));
  });
  return filteredOrders;
}
/// Add your printOrders() function below:
let printOrders = function(restaurants: Restaurant[], eligibleOrders: Order[][]): void {
  restaurants.forEach((restaurant, index) => {
    if(eligibleOrders[index].length == 0) return;
    console.log(restaurant.name);
    eligibleOrders[index].forEach(order => {
      console.log(`- ${order.name}: ${order.price}`);
    })
  })
}
/// Main
const elligibleOrders = getOrders(PriceBracket.Low, orders);
console.log(elligibleOrders);
printOrders(restaurants, elligibleOrders);
