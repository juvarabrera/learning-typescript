import products from './products';

let productName: string = "tote bag";

let shipping: number;
let taxPercent: number;
let taxTotal: number;
let total: number;
let shippingAddress: string = "Philippines";

let product;

products.forEach(p => {
  if(p.name != productName)
    return false;
  product = p;
});

console.log(product);
if(product.preOrder == "true") {
  console.log("Product on the way...")
}
if(parseInt(product.price) > 25){
  shipping = 0;
  console.log("Shipping is free for this product.");
} else {
  shipping = 5;
}

taxPercent = (shippingAddress.match("New York")) ? 0.1: 0.05;

taxTotal = parseInt(product.price) * taxPercent;

total = parseInt(product.price) + taxTotal + shipping;

console.log(`Product Name: ${product.name}`);
console.log(`Shipping Address: ${shippingAddress}`);
console.log(`Price: ${product.price}`);
console.log(`Tax Total: ${taxTotal}`);
console.log(`Shipping: ${shipping}`);
console.log(`Total amount: ${total}`);