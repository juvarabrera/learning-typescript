import courses from './courses';
import studyGroups from './studyGroups';

type Course = {
  id: number;
  studyGroupId: number;
  title: string;
  keywords: string[];
  eventType: string;
}
type StudyGroup = {
  id: number;
  courseId: number;
  title: string,
  keywords: string[],
  eventType: string,
}

type SearchEventsOptions = {
  query: string | number;
  eventType: 'courses' | 'groups';
}

let searchEvents = (options: SearchEventsOptions) => {
  let events: (Course | StudyGroup)[] = (options.eventType === 'courses') ? courses : studyGroups;
  return events.filter(event => {
    if(typeof options.query === 'number') return event.id === options.query;
    if(typeof options.query === 'string') return event.keywords.includes(options.query);
  });
}

let enrolledEvents: (Course | StudyGroup)[] = [];

let enroll = (event: Course | StudyGroup) => {
  enrolledEvents.push(event);
}

let searchOutput = searchEvents({
  query: 'art',
  eventType: 'courses'
});
console.log(searchOutput);
if(searchOutput.length != 0)
  enroll(searchOutput[0]);
console.log(enrolledEvents);